# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the krecorder package.
# Paolo Zamponi <feus73@gmail.com>, 2020, 2021, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: krecorder\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-05 12:41+0000\n"
"PO-Revision-Date: 2022-11-27 08:12+0100\n"
"Last-Translator: Paolo Zamponi <feus73@gmail.com>\n"
"Language-Team: Italian <kde-i18n-it@kde.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 22.08.3\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Paolo Zamponi"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "feus73@gmail.com"

#: contents/ui/DefaultPage.qml:23 contents/ui/PlayerPage.qml:31
#: contents/ui/RecordingListPage.qml:44
#: contents/ui/settings/SettingsDialog.qml:14
#: contents/ui/settings/SettingsPage.qml:14
#: contents/ui/settings/SettingsWindow.qml:13
#, kde-format
msgid "Settings"
msgstr "Impostazioni"

#: contents/ui/DefaultPage.qml:35
#, kde-format
msgid "Play a recording, or record a new one"
msgstr "Riproduci una registrazione, o registrane una nuova"

#: contents/ui/DefaultPage.qml:35
#, kde-format
msgid "Record a new recording"
msgstr "Registra un'altra registrazione"

#: contents/ui/main.qml:23
#, kde-format
msgid "Recorder"
msgstr "Registratore"

#: contents/ui/PlayerPage.qml:20
#, kde-format
msgid "Player"
msgstr "Lettore"

#: contents/ui/PlayerPage.qml:56
#, kde-format
msgid "Recorded on %1"
msgstr "Registrata su %1"

#: contents/ui/PlayerPage.qml:82 contents/ui/RecordPage.qml:83
#, kde-format
msgid "Pause"
msgstr "Pausa"

#: contents/ui/PlayerPage.qml:82
#, kde-format
msgid "Play"
msgstr "Riproduci"

#: contents/ui/PlayerPage.qml:92
#, kde-format
msgid "Stop"
msgstr "Ferma"

#: contents/ui/RecordingListDelegate.qml:75
#: contents/ui/RecordingListPage.qml:173
#, kde-format
msgid "Export to location"
msgstr "Esporta alla posizione"

#: contents/ui/RecordingListDelegate.qml:83
#, kde-format
msgid "Rename"
msgstr "Rinomina"

#: contents/ui/RecordingListDelegate.qml:91
#: contents/ui/RecordingListPage.qml:188 contents/ui/RecordPage.qml:115
#, kde-format
msgid "Delete"
msgstr "Elimina"

#: contents/ui/RecordingListPage.qml:21
#, kde-format
msgid "Recordings"
msgstr "Registrazioni"

#: contents/ui/RecordingListPage.qml:36 contents/ui/RecordingListPage.qml:179
#, kde-format
msgid "Edit"
msgstr "Modifica"

#: contents/ui/RecordingListPage.qml:50
#, kde-format
msgid "Record"
msgstr "Registra"

#: contents/ui/RecordingListPage.qml:112
#, kde-format
msgid "No recordings"
msgstr "Nessuna registrazione"

#: contents/ui/RecordingListPage.qml:150
#, kde-format
msgid "Select a location to save recording %1"
msgstr "Seleziona una posizione per salvare la registrazione %1"

#: contents/ui/RecordingListPage.qml:160
#, kde-format
msgid "Saved recording to %1"
msgstr "Registrazione salvata in %1"

#: contents/ui/RecordingListPage.qml:213
#, kde-format
msgid "Delete %1"
msgstr "Elimina %1"

#: contents/ui/RecordingListPage.qml:214
#, kde-format
msgid ""
"Are you sure you want to delete the recording %1?<br/>It will be "
"<b>permanently lost</b> forever!"
msgstr ""
"Sei sicuro di voler eliminare la registrazione %1?<br/>Verrà <b>persa per "
"sempre</b>."

#: contents/ui/RecordingListPage.qml:218
#, kde-format
msgctxt "@action:button"
msgid "Delete"
msgstr "Elimina"

#: contents/ui/RecordingListPage.qml:229
#, kde-format
msgctxt "@action:button"
msgid "Cancel"
msgstr "Annulla"

#: contents/ui/RecordingListPage.qml:241
#, kde-format
msgid "Rename %1"
msgstr "Rinomina %1"

#: contents/ui/RecordingListPage.qml:256 contents/ui/RecordPage.qml:165
#, kde-format
msgid "Name:"
msgstr "Nome:"

#: contents/ui/RecordingListPage.qml:262
#, kde-format
msgid "Location:"
msgstr "Posizione:"

#: contents/ui/RecordPage.qml:21
#, kde-format
msgid "Record Audio"
msgstr "Registra audio"

#: contents/ui/RecordPage.qml:83
#, kde-format
msgid "Continue"
msgstr "Continua"

#: contents/ui/RecordPage.qml:97
#, kde-format
msgid "Stop Recording"
msgstr "Ferma la registrazione"

#: contents/ui/RecordPage.qml:135
#, kde-format
msgid "Save recording"
msgstr "Salva registrazione"

#: contents/ui/RecordPage.qml:139
#, kde-format
msgid "Save"
msgstr "Salva"

#: contents/ui/RecordPage.qml:151
#, kde-format
msgid "Discard"
msgstr "Scarta"

#: contents/ui/RecordPage.qml:166
#, kde-format
msgid "Name (optional)"
msgstr "Nome (facoltativo)"

#: contents/ui/RecordPage.qml:170
#, kde-format
msgid "Storage Folder:"
msgstr "Cartella di memorizzazione:"

#: contents/ui/settings/SettingsComponent.qml:33
#, kde-format
msgid "General"
msgstr "Generale"

#: contents/ui/settings/SettingsComponent.qml:39
#, kde-format
msgid "About"
msgstr "Informazioni su"

#: contents/ui/settings/SettingsComponent.qml:57
#, kde-format
msgid "Audio Format"
msgstr "Formato audio"

#: contents/ui/settings/SettingsComponent.qml:59
#, kde-format
msgctxt "Ogg Vorbis file format"
msgid "Ogg Vorbis"
msgstr "Ogg Vorbis"

#: contents/ui/settings/SettingsComponent.qml:59
#, kde-format
msgctxt "Ogg Opus file format"
msgid "Ogg Opus"
msgstr "Ogg Opus"

#: contents/ui/settings/SettingsComponent.qml:59
#, kde-format
msgctxt "FLAC file format"
msgid "FLAC"
msgstr "FLAC"

#: contents/ui/settings/SettingsComponent.qml:59
#, kde-format
msgctxt "MP3 file format"
msgid "MP3"
msgstr "MP3"

#: contents/ui/settings/SettingsComponent.qml:59
#, kde-format
msgid "WAV file format"
msgstr "Formato di file WAV"

#: contents/ui/settings/SettingsComponent.qml:59
#, kde-format
msgctxt "File format not listed"
msgid "Other"
msgstr "Altro"

#: contents/ui/settings/SettingsComponent.qml:86
#, kde-format
msgid "Audio Quality"
msgstr "Qualità audio"

#: contents/ui/settings/SettingsComponent.qml:87
#, kde-format
msgid "Lowest"
msgstr "Minima"

#: contents/ui/settings/SettingsComponent.qml:87
#, kde-format
msgid "Low"
msgstr "Bassa"

#: contents/ui/settings/SettingsComponent.qml:87
#, kde-format
msgid "Medium"
msgstr "Media"

#: contents/ui/settings/SettingsComponent.qml:87
#, kde-format
msgid "High"
msgstr "Alta"

#: contents/ui/settings/SettingsComponent.qml:87
#, kde-format
msgid "Highest"
msgstr "Massima"

#: contents/ui/settings/SettingsComponent.qml:88
#, kde-format
msgid "Higher audio quality also increases file size."
msgstr "Una qualità alta aumenta anche la dimensione del file."

#: contents/ui/settings/SettingsComponent.qml:113
#, kde-format
msgid "Advanced"
msgstr "Avanzate"

#: contents/ui/settings/SettingsComponent.qml:119
#, kde-format
msgid "Audio Input"
msgstr "Ingresso audio"

#: contents/ui/settings/SettingsComponent.qml:152
#, kde-format
msgid "Audio Codec"
msgstr "Codec audio"

#: contents/ui/settings/SettingsComponent.qml:185
#, kde-format
msgid "Container Format"
msgstr "Formato contenitore"

#: contents/ui/settings/SettingsComponent.qml:216
#, kde-format
msgid ""
"Some combinations of codecs and container formats may not be compatible."
msgstr ""
"Alcune combinazioni fra codec e formati contenitori potrebbero non essere "
"compatibili."

#: main.cpp:53
#, kde-format
msgid "© 2020-2022 KDE Community"
msgstr "© 2020-2022 Comunità KDE"

#: main.cpp:54
#, kde-format
msgid "Devin Lin"
msgstr "Devin Lin"

#: main.cpp:55
#, kde-format
msgid "Jonah Brüchert"
msgstr "Jonah Brüchert"

#~ msgid "Custom"
#~ msgstr "Personalizzato"

#~ msgid "WAV"
#~ msgstr "WAV"

#~ msgid "%1"
#~ msgstr "%1"

#~ msgid "No recordings yet, record your first!"
#~ msgstr "Ancora nessuna registrazione, fai la prima."

#~ msgid "Editing %1"
#~ msgstr "Modifica di %1"

#~ msgid "Audio Input:"
#~ msgstr "Ingresso audio:"

#~ msgid "<b>New Recording</b>"
#~ msgstr "<b>Nuova registrazione</b>"

#~ msgid "<b>Settings</b>"
#~ msgstr "<b>Impostazioni</b>"

#~ msgid "Hide Advanced Settings"
#~ msgstr "Nascondi impostazioni avanzate"

#~ msgid "Show Advanced Settings"
#~ msgstr "Mostra impostazioni avanzate"

#~ msgid "Delete recording"
#~ msgstr "Elimina registrazione"

#~ msgctxt "@action:button"
#~ msgid "Done"
#~ msgstr "Fatto"
